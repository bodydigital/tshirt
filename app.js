app.controller('appController', ['$scope','$http','$timeout', '$q','$log', '$window', function($scope, $http, $timeout, $q, $log, $window) {
     $scope.page='main';
     $scope.otherProducts = [];
     $scope.imagePath="assets/img/contactos/60.jpeg";
     $scope.contactos = [
        {
          face : $scope.imagePath,
          what: 'Telefono de contacto',
          who: '4884448488'
        
        },
        {
          face : $scope.imagePath,
          what: 'Telefono de contacto',
          who: '4884448488'
        },
        {
          face : $scope.imagePath,
          what: 'Telefono de contacto',
          who: '4884448488'
        },
        {
          face : $scope.imagePath,
          what: 'Telefono de contacto',
          who: '4884448488'
        },
        {
          face : $scope.imagePath,
          what: 'Telefono de contacto',
          who: '4884448488'
        }
      ];
     $scope.products=[
                  {
                      "id": 1,
                      "name": "nombre usuario",
                      "categorie": "Herrero",
                      "description": "Tenetur architecto harum iusto soluta, quae doloremque.",
                      "price": 22,
                      "image":'s.jpg'
          
                  },
                  {
                      "id": 2,
                      "name": "nombre usuario",
                      "categorie": "Jardinero",
                      "description": "Tenetur architecto harum iusto soluta, quae doloremque.",
                      "price": 19,
                      "image":'s.jpg'
                  },
                  {
                      "id": 3,
                      "name": "nombre usuario",
                      "categorie": "Fontanero",
                      "description": "Tenetur architecto harum iusto soluta, quae doloremque.",
                      "price": 19,
                      "image":'s.jpg'
                  },
                  {
                      "id": 4,
                      "name": "nombre usuario",
                      "categorie": "Electricista",
                      "description": "Tenetur architecto harum iusto soluta, quae doloremque.",
                      "price": 22,
                      "image":'s.jpg'
                  },
                  {
                      "id": 5,
                      "name": "nombre usuario",
                      "categorie": "Mecanico",
                      "description": "Tenetur architecto harum iusto soluta, quae doloremque.",
                      "price": 15,
                      "image":'s.jpg'
                  },
                  {
                    "id": 6,
                    "name": "nombre usuario",
                    "categorie": "Arquitecto",
                    "description": "Tenetur architecto harum iusto soluta, quae doloremque.",
                    "price": 22,
                    "image":'s.jpg'
                },
                {
                    "id": 7,
                    "name": "nombre usuario",
                    "categorie": "Abogado",
                    "description": "Tenetur architecto harum iusto soluta, quae doloremque.",
                    "price": 15,
                    "image":'s.jpg'
                },
                {
                  "id": 8,
                  "name": "nombre usuario",
                  "categorie": "Pintor",
                  "description": "Tenetur architecto harum iusto soluta, quae doloremque.",
                  "price": 22,
                  "image":'s.jpg'
              },
              {
                  "id": 9,
                  "name": "nombre usuario",
                  "categorie": "Escultor",
                  "description": "Tenetur architecto harum iusto soluta, quae doloremque.",
                  "price": 15,
                  "image":'s.jpg'
              },
              {
                "id": 10,
                "name": "nombre usuario",
                "categorie": "Desarrollador web",
                "description": "Tenetur architecto harum iusto soluta, quae doloremque.",
                "price": 22,
                "image":'s.jpg'
            },
            {
                "id": 11,
                "name": "nombre usuario",
                "categorie": "Programador",
                "description": "Tenetur architecto harum iusto soluta, quae doloremque.",
                "price": 15,
                "image":'s.jpg'
            }
          ];
          
   
          $scope.prepareProducts= function(prods) {
            var columns = [
                [],
                [],
                [],
                []
            ];
            var i = 0;
            if (prods.length == 0) prods = $scope.otherProducts;
            // prods = prods.filter(pp => pp.published);
            prods.forEach(function (prod) {
                if (i == 0) {
                    columns[0].push(prod);
                    i = 1;
                } else if (i == 1) {
                    columns[1].push(prod);
                    i = 2;
                } else if (i == 2) {
                    columns[2].push(prod);
                    i = 3;
                } else {
                    columns[3].push(prod);
                    i = 0;
                }
            })
            $scope.productsMobile = prods;
            $scope.products = columns;



        };
        $scope.prepareProducts($scope.products);

        $scope.resized = window.innerWidth <= 1050 ? true : false;
    
        var old = $scope.resized;
    
        angular.element($window).bind('resize', function () {
            $scope.resized = window.innerWidth <= 1050 ? true : false;
            if ($scope.resized != old) {
                $timeout(function () {
                    $('.products').addClass('show');
                }, 1000)
                old = $scope.resized;
            }
            $scope.$apply();
        }); 

        $scope.goTop=function(){
            $window.scrollTo(0, 0);
        }
        
    angular.element($window).bind("scroll", function () {
        var y = this.pageYOffset;
        $scope.scrolled = false;
        if($scope.page=='main'){
            if (y > 100) {
                $scope.scrolled = true;
                $("#mainNav").addClass("navbar-shrink");
            }else{
                $("#mainNav").removeClass("navbar-shrink");
            }
        }else{
            $("#mainNav").addClass("navbar-shrink");
        }

       
        $scope.$apply();
    });

       $scope.mostrarServicios=function(){

            $timeout(function () {
                $timeout(function () {
                    $('.bg-rectangle').addClass('animate__animated animate__slideInLeft');
                    $timeout(function () {
                        $('.products').addClass('show');
                    }, 1000);
                }, 1000);
            }, 1000);

       };
       $scope.mostrarServicios();

       $scope.mostrarinicio=function(){
           $scope.goTop();
           $("#mainNav").removeClass("navbar-shrink");
        $timeout(function () {
            $scope.activoOff=true;
            $timeout(function () { 
               $scope.onOff=true;
               $timeout(function () { 
                $scope.navb=true;
                
             }, 800);
            }, 800);

        }, 500)

       };
       $scope.mostrarinicio();

    $scope.datos=[];
    // consultar
    $scope.select = function(identificador) {
        $http.get(identificador, {})
        .then(function success(e) {
            $scope.datos = e.data;
        }, function error(e) {
            console.log("Se ha producido un error al recuperar la información");
        });
    return $scope.datos;
    };
    $scope.select('https://area-guide-service.firebaseio.com/negocios.json');


}]);
          
app.directive('product',
    function ($timeout) {
        return {
            restrict: 'A',
            link: function ($scope, $element) {
                let parent = $element.parent().data('index') + 1;
                let me = $scope.$index;
                let time = (me * 4 + parent) / 5;
                $element.addClass('show');
                $element.css('transition-delay', time + 's');
                $element.find('.bg').css('transition-delay', time + 0.1 + 's');
            }
        };
    });
    
