var app = angular.module('app', ['ui.router']);
app.config(function($stateProvider) {

    var states = [
        {
            name: 'main',
            url: '/Inicio',
            templateUrl: 'views/main.html'
        },
        {
            name: 'contacto',
            url: '/Contacto',
            templateUrl: 'views/contacto.html'
        }
    ];

    states.forEach(function(state) {
        $stateProvider.state(state);
      });
      
});
app.run(function($state) {
     $state.go('main');
});